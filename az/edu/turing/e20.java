package az.edu.turing;

import java.util.Scanner;

public class e20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = 0;
        for (; a <= 0; b++) ;
        int sum = 0;
        int c = a;
        while (c != 0) {
            sum += c % 10;
            c /= 10;
        }
        a -= sum;
        {
            System.out.println(a);
        }
    }
}