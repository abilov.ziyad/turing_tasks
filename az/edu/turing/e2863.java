package az.edu.turing;

import java.util.Scanner;

public class e2863 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int count = 0;
        for (int i = 1; i <= a; i++) {
            if (a % i == 0 && i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}
