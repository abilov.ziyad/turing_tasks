package az.edu.turing;

import java.util.Scanner;

public class e622 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        int count = 0;

        String binaryString = Integer.toBinaryString(a);
        for (int i = 0; i < binaryString.length(); i++) {
            if (binaryString.charAt((int) i) == '1') {
                count++;
            }
        }

        System.out.println(count);

    }
}


